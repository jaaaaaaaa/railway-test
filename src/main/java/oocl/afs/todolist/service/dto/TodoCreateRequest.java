package oocl.afs.todolist.service.dto;

public class TodoCreateRequest {
    private String name;

    public TodoCreateRequest() {
    }

    public String getText() {
        return name;
    }

    public void setText(String text) {
        this.name = text;
    }
}
